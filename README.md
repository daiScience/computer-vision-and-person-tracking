# Computer vision and person tracking

This repository implements a simple approach for person tracking on a video stream.
Execution time is very fast and makes it ideal for an cheap and homemade CCTV
The implementation includes

1. Classes for manipulation of person, video stream and tracker 
2. Example video to test the output and calibration parameters


## Simplified algorithm

The difference between current frame and the base frame, the image background, gives a view of moving objects.   
Any moving object big enough is considered a person with the center of the bounding square used as approximation of the person position.   
The persons recognised in current frame are compared to previously recognised persons stored in a holder object.    
The tracker is updated based on distance between existing and new persons on frame and time unseen on the video stream.    
Any person not seen for a number of frame is considered gone and removed from the holder while close enough persons are associated.          

![](https://bitbucket.org/daiScience/computer-vision-and-person-tracking/downloads/Capture.png)

The approach is parametric and calibrations required are 

1. *width*: represent the rescale width of the video stream to speed the processing time and simplify other parameter calibration
2. *min_area*: represent the minimum area to considered a moving object a person, used to filter noise 
3. *max_distance*: represent the maximum distance to associate together person from one frame to the next versus considering one new person
4. *max_unseen*: represent the number of frame an object can disappear from the video before being considered gone  
2. *threshold*: is the image thresholding parameter of opencv, in doubt leave to 50 

Note  

* At the expense of speed, *Stream.detect* can be overloaded to using a proper neural network trained for human recognition to confirm moving object is a person
* A subclass of *Tracker* can be created to implement a person counter depending on current location of a person using *center* and *previous* attributes of Person    

## Usage 

Example of configuration to process an existing video file without saving any video output   
*Note pressing CRTL+C once will exit the process as well as release video and/or webcam*

~~~~python3
    param = dict(
        input = r'example.mp4'    # video file input, None for webcam
        , output = None           # video file output, None for no video output
        , max_unseen = 30         # number of missing frame before considering gone
        , width = 500             # width to resize the image to
        , max_distance = 10000    # max distance between frame to associate person together 
        , base = None             # base background frame, None for first frame 
        , threshold = 50          # detection threshold for cv2.threshold
        , min_area = 4000         # minimum area detected to consider 
        )
    tracker = Tracker(**param)    # initialise the tracker 
    tracker.update()              # update the tracker by processing frames
~~~~  


Example of configuration to use the webcam for tracking and saving an output file. The first frame will be used for background unless *base* is passed        
*Note pressing CRTL+C once will exit the process as well as release video and/or webcam*

~~~~python3
    param = dict(
        input = None              # video file input, None for webcam
        , output = r'webcam.avi'  # video file output, None for no video output
        , max_unseen = 30         # number of missing frame before considering gone
        , width = 500             # width to resize the image to
        , max_distance = 10000    # max distance between frame to associate person together 
        , base = None             # base background frame, None for first frame 
        , threshold = 50          # detection threshold for cv2.threshold
        , min_area = 4000         # minimum area detected to consider 
        )
    tracker = Tracker(**param)    # initialise the tracker 
    tracker.update()              # update the tracker by processing frames
~~~~
