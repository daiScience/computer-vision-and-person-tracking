# -*- coding: utf-8 -*-
'''
tracking.py
~~~~~~~~~~~
This module implements person tracking on a video
Simple but fast algo ideal for homemade cctv
'''

import logging       # logging facility
import collections   # high-performance container datatypes
import scipy.spatial # spatial algorithms and data structures
import numpy         # multi-dimensional arrays and matrices
import imutils       # basic image processing
import imutils.video # basic image processing
import cv2           # opencv api
import time          # time access and conversions
import datetime      # basic date and time types
import functools     # higher-order functions and operations
import os            # misc operating system interfaces
import inspect       # inspect live objects
import errno         # standard errno system symbols

logging_format = '%(processName)s - %(asctime)s - %(filename)s '
logging_format += '- %(funcName)s - %(lineno)s - %(levelname)s - %(message)s'
logging.basicConfig(format=logging_format)
logger_module = logging.getLogger(__name__)
logger_module.setLevel(logging.INFO)

def where(depth=1):
    ''' Return location where the code is running '''
    return os.path.dirname(os.path.abspath(inspect.stack()[depth][1])) if depth else None

class Person:
    ''' Representation of a person in an image
    :rectangle: tuple of tuple of ints representing person boundary
    :unseen: number of frame unseen
    :counted: whether or not person has been counted or not
    :center: tuple of ints representing person current center
    :area: area of the rectangle
    :previous: list of previous centers
    '''
    def __init__(self, point1, point2, **kwargs):
        ''' Initialise a person object '''
        self.rectangle = (point1, point2)
        self.unseen = 0
        self.counted =False
        self.center = self.get_center()
        self.area = self.get_area()
        self.previous = []
        return None

    @classmethod
    def from_square(cls, square):
        ''' Initilise from 4 value point square '''
        (x1, y1, x2, y2) = square
        return cls((x1, y1), (x2, y2))

    def to_square(self):
        ''' Convert to 4 value point square '''
        (x1, y1), (x2, y2) = self.rectangle
        return (x1, y1, x2, y2)

    def get_center(self):
        ''' Calculate center of rectangle '''
        (x1, y1), (x2, y2) = self.rectangle
        return ((x1+x2) // 2, (y1+y2) // 2)

    def update(self, rectangle):
        ''' Update position of person '''
        self.previous.append(self.center)
        self.rectangle = rectangle
        self.center = self.get_center()
        self.area = self.get_area()
        self.unseen = 0
        return self

    def get_area(self):
        (x1, y1), (x2, y2) = self.rectangle
        return abs(x1-x2)*abs(y1-y2)

    def __str__(self):
        ''' Person to string '''
        return str(self.__class__.__name__)+str((self.center, self.area))
    __repr__ = __str__

    def __eq__(self, other):
        ''' Equality between person '''
        return all(getattr(self, att)==getattr(other, att) for att in self.__dict__)

    def __bool__(self):
        ''' Person to boolean '''
        return self.counted

class Holder:
    ''' Representation of an object holding tracked objects
    :counter: helper holding number of tracked objects
    :holded: ordered dictionary containing tracked objects
    :max_unseen: number of frame to wait until an unseen object is lost
    :max_distance: distance in pixel to associate existing object together
    '''
    def __init__(self, max_unseen=30, max_distance=50, **kwargs):
        ''' Initialise a holder
        :max_unseen: number of frame to wait until unseen person is lost
        :max_distance: distance in pixel to associate existing person together
        '''
        self.max_unseen = max_unseen
        self.max_distance = max_distance
        self.counter = 0
        self.holded = collections.OrderedDict()
        return None

    def __str__(self):
        ''' Person to string '''
        return str(self.__class__.__name__)+str((self.counter, self.holded))
    __repr__ = __str__

    def __bool__(self):
        ''' Holder is empty '''
        return bool(self.holded)

    def __len__(self):
        ''' Length of holder '''
        return len(self.holded)

    def add(self, holded):
        ''' Start holding object '''
        self.holded[self.counter] = holded
        self.counter += 1
        return self

    def remove(self, index):
        ''' Stop holding object '''
        del self.holded[index]
        return self

    def distance(self, another, return_index=False):
        ''' Distance matrix between holded and another object list '''
        track = [each.center for each in self.holded.values()]
        other = [each.center for each in another]
        d = scipy.spatial.distance.cdist(track, other)
        return (d, *self.get_indexes(d)) if return_index else d

    @staticmethod
    def get_indexes(distance):
        ''' Unique row and col indexes of closest pairs '''
        ind_rows, ind_cols = [], []
        ind = distance.argsort(axis=None)
        rows, cols = numpy.unravel_index(ind, distance.shape)
        for r, c in zip(rows, cols):
            if r in ind_rows or c in ind_cols:
                continue
            ind_rows.append(r)
            ind_cols.append(c)
        return ind_rows, ind_cols

    def merge_index(self, distance, persons, rows, cols, logger=logger_module):
        ''' Merge holded and persons given closest indexes '''
        ids = list(self.holded.keys())
        done_rows, done_cols = set(), set()
        for (row, col) in zip(rows, cols):
            if logger:
                show = (ids[row], col, self.max_distance)
                logger.debug('ids[row]=%s, col=%s, self.max_distance=%s' % show)
            if row in done_rows or col in done_cols:
                continue
            if self.max_distance and distance[row, col] > self.max_distance:
                continue
            self.holded[ids[row]].update(persons[col].rectangle)
            done_rows.add(row)
            done_cols.add(col)
        return done_rows, done_cols

    def leftover_index(self, distance, persons, done_rows, done_cols, logger=logger_module):
        ''' Update holded with persons given unallocated indexes '''
        ids = list(self.holded.keys())
        undone = set(range(distance.shape[0])).difference(done_rows)
        for row in undone:
            if logger:
                logger.debug('ids[row]=%s' % ids[row])
            self.holded[ids[row]].unseen += 1
            if self.holded[ids[row]].unseen > self.max_unseen:
                self.remove(ids[row])
        undone = set(range(distance.shape[1])).difference(done_cols)
        for col in undone:
            if logger:
                logger.debug('col=%s' % col)
            self.add(persons[col])
        return self

    def merge(self, persons, logger=logger_module):
        ''' Merge existing holded object with list of objects '''
        distance, rows, cols = self.distance(persons, True)
        done_rows, done_cols = self.merge_index(distance, persons, rows, cols, logger=logger)
        self.leftover_index(distance, persons, done_rows, done_cols, logger=logger)
        return self

    def update_empty(self):
        ''' Update the holder given empty object list '''
        todel = []
        for key in list(self.holded.keys()):
            self.holded[key].unseen += 1
            if self.holded[key].unseen > self.max_unseen:
                self.remove(key)
        return self

    def update(self, another):
        ''' Update the holder object given a list of object '''
        if not another:
            return self.update_empty()
        if not self:
            for each in another:
                self.add(each)
            return self
        return self.merge(another)

class EndOfStream(Exception):
    ''' Helper exception for end of stream reached '''
    pass

class Stream:
    ''' Representation of a video stream
    :input: input file path, None for webcam
    :output: output file path, None for no output file
    :stream: video stream from file or webcam handler
    :frame: current frame read
    :base: static base frame without object
    :width: width to resize the image
    :start: datetime of stream starting
    :counter: count number of frame streamed
    :threshold: threshold for detection
    '''
    def __init__(self, input=None, output=None, base=None, width=500, threshold=50, **kwargs):
        ''' Initialise a stream object '''
        self.input = input
        self.output = output
        self.writer = None
        self.stream = None
        self.frame = None
        self.base = None
        self.threshold = threshold
        self.width = width
        self.begin = datetime.datetime.now()
        self.counter = 0
        return None

    def __bool_(self):
        ''' Stream is empty '''
        return self.frame is not None

    def __str__(self):
        ''' Person to string '''
        return str(self.__class__.__name__)+str((self.counter, self.input, self.output))
    __repr__ = __str__

    def start(self, src=0, logger=logger_module):
        ''' Start stream '''
        if self.input is None:
            self.stream = imutils.video.VideoStream(src=src)
            self.stream.start()
        else:
            if not os.path.isfile(self.input):
                input = os.path.join(where(), self.input)
                if not os.path.isfile(input):
                    raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), self.input)
                if logger:
                    logger.debug('input changed from %s to %s' % (self.input, input))
                self.input = input
            self.stream = cv2.VideoCapture(self.input)
        time.sleep(1)
        return self

    def get_frame(self):
        ''' Read a video frame '''
        frame = self.stream.read()
        frame = frame[1] if self.input else frame
        if frame is None:
            raise EndOfStream
        frame = imutils.resize(frame, width=self.width) if self.width else frame
        self.counter += 1
        return frame

    def process_frame(self, frame, color=cv2.COLOR_BGR2GRAY, kernel=(25,25)):
        ''' Process a video frame '''
        result = cv2.cvtColor(frame, color)
        result = cv2.GaussianBlur(result, kernel, 0)
        return result

    def read(self):
        ''' Read a video frame '''
        if self.stream is None:
            self.start()
        if self.base is None:
            self.base = self.get_frame()
        self.frame = self.get_frame()
        if self.frame is None:
            raise EndOfStream
        return self

    def detect(self):
        ''' Detect movement in the video stream '''
        frame, base = self.process_frame(self.frame), self.process_frame(self.base)
        delta = cv2.absdiff(frame, base)
        _, threshold = cv2.threshold(delta, self.threshold, 255, cv2.THRESH_BINARY)
        threshold = cv2.dilate(threshold, None, iterations=2)
        _, contours, _ = cv2.findContours(threshold, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        contours = [cv2.boundingRect(contour) for contour in contours]
        return [(x, y, x+w, y+h) for (x, y, w, h) in contours]

    def start_writer(self, logger=logger_module):
        ''' Start the video writer '''
        fourcc = cv2.VideoWriter_fourcc(*"MJPG")
        height, width = self.frame.shape[:2]
        self.writer = cv2.VideoWriter(self.output, fourcc, 30, (width, height), True)
        return self

    def draw(self, contours, color=(255,0,0), width=1):
        ''' Draw contour of moving objects '''
        for (x1, y1, x2, y2) in contours:
            cv2.rectangle(self.frame, (x1, y1), (x2, y2), color, width)
        return self

    def write(self, show=True, wait_time=25):
        ''' Write video frame '''
        if self.output is not None:
            if self.writer is None:
                self.start_writer()
            self.writer.write(self.frame)
        if show:
            cv2.imshow('Frame', self.frame)
            key = cv2.waitKey(wait_time) & 0xFF
        return self

    def stop(self):
        ''' Stop the stream '''
        if self.writer is not None:
            self.writer.release()
        if self.input is None:
            self.stream.stop()
        else:
            if self.stream is not None:
                self.stream.release()
        cv2.destroyAllWindows()
        return self

    def elapsed(self):
        ''' Helper for elapsed time in seconds since stream begin '''
        return (datetime.datetime.now()-self.begin).total_seconds()

    @staticmethod
    def time(format='%H:%M:%S %d/%m/%Y'):
        return datetime.datetime.now().strftime(format)
    
    def get_info(self):
        ''' Retrieve stream info '''
        return [('Time', self.time())
                , ('Elapsed', '{:.2f}'.format(self.elapsed()))
                , ('FPS', '{:.2f}'.format(self.counter/self.elapsed()))]

    def info(self, color=(0, 0, 255), width=1, gap=15):
        ''' Draw stream info '''
        info = self.get_info()
        for (i, (k, v)) in enumerate(info):
            text = "{}: {}".format(k, v)
            loc = (gap, (i*gap)+gap)
            cv2.putText(self.frame, text, loc, cv2.FONT_HERSHEY_SIMPLEX, 0.4, color, width)
        return self

    @staticmethod
    def non_overlapping(contours, threshold=0.1):
        ''' Suppression of overlapping contours
        https://www.pyimagesearch.com/2015/02/16/faster-non-maximum-suppression-python/
        '''
        if len(contours)==0:
            return []
        if threshold is None or threshold<=0:
            return contours
        pick = []
        x1, y1, x2, y2 = numpy.array(list(zip(*contours)))
        area = (x2 - x1 + 1) * (y2 - y1 + 1)
        idxs = numpy.argsort(y2)
        while len(idxs) > 0:
            last = len(idxs) - 1
            i = idxs[last]
            pick.append(i)
            xx1 = numpy.maximum(x1[i], x1[idxs[:last]])
            yy1 = numpy.maximum(y1[i], y1[idxs[:last]])
            xx2 = numpy.minimum(x2[i], x2[idxs[:last]])
            yy2 = numpy.minimum(y2[i], y2[idxs[:last]])
            w = numpy.maximum(0, xx2 - xx1 + 1)
            h = numpy.maximum(0, yy2 - yy1 + 1)
            overlap = (w * h) / area[idxs[:last]]
            idxs = numpy.delete(idxs, numpy.concatenate(([last],
                numpy.where(overlap > threshold)[0])))
        return numpy.array(contours)[pick].astype("int").tolist()

    def update_frame(self):
        ''' Update the stream with a frame '''
        self.stream.read()
        moving = self.non_overlapping(moving)
        self.stream.draw(moving)
        self.stream.info()
        self.stream.write()
        return self
        
    def update(self):
        ''' Read frame, detect and draw objects then write frame '''
        try:
            while True:
                self.update_frame()
            return self
        except EndOfStream as e:
            pass
        except KeyboardInterrupt as e:
            pass
        finally:
            if stream is not None:
                stream.stop()
        return self

class Tracker:
    ''' Representation of a tracker
    :stream: Stream representing video stream to track
    :holder: Holder containing live trackable objects
    :min_area: minimum area to consider a moving object
    '''
    def __init__(self, min_area=None, **kwargs):
        ''' Initialise a tracker '''
        self.stream = Stream(**kwargs)
        self.holder = Holder(**kwargs)
        self.min_area = min_area
        return None

    def __str__(self):
        ''' Person to string '''
        return str(self.__class__.__name__)+str((self.stream, self.holder))
    __repr__ = __str__

    def __bool__(self):
        ''' Tracker to boolean '''
        return bool(self.stream)

    def draw(self, color=(255,0,0)):
        ''' Draw the tracker '''
        for key, person in self.holder.holded.items():
            frame = self.stream.frame
            c1, c2 = person.center
            cv2.circle(frame, (c1, c2), 4, color, -1)
            text = "ID {}".format(key)
            cv2.putText(frame, text, (c1-10, c2-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 1)
            (x1, y1, x2, y2) = person.to_square()
            cv2.rectangle(frame, (x1, y1), (x2, y2), color, 1)
        return self

    def update_frame(self, draw_moving=False):
        ''' Update one tracker frame '''
        self.stream.read()
        moving = self.stream.detect()
        moving = self.stream.non_overlapping(moving)
        persons = [Person.from_square(contour) for contour in moving]
        if self.min_area:
            persons = [person for person in persons if person.area>=self.min_area]
        if draw_moving:
            self.stream.draw(moving, color=(155,155,155), width=5)
        self.holder.update(persons)
        self.draw()
        self.stream.info()
        self.stream.write()
        return self
    
    def update(self, draw_moving=False):
        ''' Update tracker '''
        try:
            while True:
                self.update_frame(draw_moving=draw_moving)
            return self
        except EndOfStream as e:
            pass
        except KeyboardInterrupt as e:
            pass
        finally:
            if self.stream:
                self.stream.stop()
        return self

if __name__=='__main__':

    param = dict(
        input = r'example.mp4'    # video file input, None for webcam
        , output = None           # video file output, None for no video output
        , max_unseen = 30         # number of missing frame before considering gone
        , width = 500             # width to resize the image to
        , max_distance = 10000    # max distance between frame to associate person together 
        , base = None             # base background frame, None for first frame 
        , threshold = 50          # detection threshold for cv2.threshold
        , min_area = 4000         # minimum area detected to consider 
        )
    tracker = Tracker(**param)    # initialise the tracker 
    tracker.update()              # update the tracker by processing frames
