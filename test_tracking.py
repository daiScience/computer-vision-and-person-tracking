# -*- coding: utf-8 -*-
'''
test_tracking.py
~~~~~~~~~~~~~~~~~~~~~
This module implements test of tracker module
'''

import unittest      # unit testing framework
import functools     # higher-order functions and operations 
import io            # core tools for working with streams
import warnings      # warning control
import sys           # system-specific parameters and functions

import tracking      # user-defined, person tracking on a video

def ignore_warnings(fn):
    ''' Helper decorator to remove warnings in unit tests '''
    @functools.wraps(fn)
    def do_test(self, *args, **kwargs):
        with warnings.catch_warnings():
            warnings.simplefilter('ignore')
            fn(self, *args, **kwargs)
    return do_test

def silence_prints(fn):
    class NullIO(io.StringIO):
        ''' Helper class to silent function '''
        def write(self, txt):
            pass
    ''' Helper decorator to prevent functions to print in unit tests '''
    @functools.wraps(fn)
    def do_test(*args, **kwargs):
        saved_stdout = sys.stdout
        sys.stdout = NullIO()
        try:
            result = fn(*args, **kwargs)
        except Exception as e:
            raise e
        finally:
            sys.stdout = saved_stdout
        return result
    return do_test

def clean_unittest(fn):
    ''' Helper decorator to prevent functions to print and remove warnings '''
    @functools.wraps(fn)
    @ignore_warnings
    @silence_prints
    def do_test(*args, **kwargs):
        return fn(*args, **kwargs)
    return do_test
    
class TestPersonTracker(unittest.TestCase):

    @clean_unittest
    def test01_Person01(self):
        ''' Test base person class '''
        bottom_left, top_right = (1,1), (3,3)
        person = tracking.Person(bottom_left, top_right)
        self.assertTrue(person.rectangle==(bottom_left, top_right))
        self.assertTrue(person.center==(2,2))
        self.assertTrue(person.unseen==0)
        self.assertFalse(person.counted)
        self.assertTrue(person.previous==[])
        
    @clean_unittest
    def test02_Person02(self):
        ''' Test person update '''
        bottom_left, top_right = (1,1), (3,3)
        person = tracking.Person(bottom_left, top_right)
        person.unseen = 10
        new_rectangle = ((5,5), (3,3))
        person.update(new_rectangle)
        self.assertTrue(person.rectangle==new_rectangle)
        self.assertTrue(person.center==(4,4))
        self.assertTrue(person.unseen==0)
        self.assertFalse(person.counted)
        self.assertTrue(person.previous==[(2,2)])
        
    @clean_unittest
    def test03_Holder01(self):
        ''' Test base holder class '''
        person0 = tracking.Person((1,1), (3,3))
        person1 = tracking.Person((7,9), (8,10))
        person2 = tracking.Person((0,0), (0,0))
        holder = tracking.Holder()
        self.assertTrue(holder.counter==0)
        self.assertFalse(holder)
        holder.add(person0)
        self.assertTrue(holder.counter==1)
        self.assertTrue(holder)
        self.assertTrue(len(holder)==1)
        self.assertTrue(holder.holded[0] is person0)
        holder.add(person1)
        holder.add(person2)
        self.assertTrue(holder.counter==3)
        self.assertTrue(holder)
        self.assertTrue(len(holder)==3)
        self.assertTrue(holder.holded[1] is person1)
        self.assertTrue(holder.holded[2] is person2)
        holder.remove(2)
        self.assertTrue(holder.counter==3)
        self.assertTrue(len(holder)==2)

    @clean_unittest
    def test04_Holder02(self):
        ''' Check update on a empty list of objects '''
        person0 = tracking.Person((1,1), (3,3))
        person1 = tracking.Person((7,9), (9,11))
        holder = tracking.Holder()
        holder.add(person0)
        holder.add(person1)
        holder.update([])
        self.assertTrue(person0.unseen==1)
        self.assertTrue(person1.unseen==1)
        
    @clean_unittest
    def test05_Holder03(self):
        ''' Check distance holder '''
        person0 = tracking.Person((1,1), (3,3))
        person1 = tracking.Person((5,5), (7,9))
        person2 = tracking.Person((4,5), (6,7))
        person3 = tracking.Person((1,1), (1,1))
        holder = tracking.Holder()
        holder.add(person0)
        holder.add(person1)
        distance = holder.distance([person2])
        self.assertTrue(distance[0][0]==5)
        self.assertTrue(distance[1][0]==2**0.5)
        d, row, col = holder.distance([person2], return_index=True)
        self.assertTrue((distance==d).all())
        self.assertTrue(row==[1])
        self.assertTrue(col==[0])
        d, row, col = holder.distance([person2, person3], return_index=True)
        self.assertTrue(row==[0,1])
        self.assertTrue(col==[1,0])
    
    @clean_unittest
    def test06_Holder04(self):
        ''' Test update holder with one object '''
        person0 = tracking.Person((1,1), (3,3))
        person1 = tracking.Person((5,5), (7,9))
        person2 = tracking.Person((4,4), (6,6))
        person3 = tracking.Person((1,1), (1,1))
        holder = tracking.Holder()
        holder.add(person0)
        holder.add(person1)
        holder.update([person2])
        self.assertTrue(len(holder)==2)
        self.assertTrue(holder.holded[1].previous==[(6,7)])
        self.assertTrue(holder.holded[1].center==(5,5))
        self.assertTrue(holder.holded[0] is person0)
        self.assertTrue(holder.holded[1] is person1)
        self.assertTrue(person0.unseen==1)
        self.assertTrue(person1.unseen==0)
        self.assertFalse(person1 is person2)
        self.assertFalse(person1==person2)
        self.assertTrue(person1.center==person2.center)
        self.assertTrue(person1.rectangle==person2.rectangle)
        self.assertFalse(person1.previous==person2.previous)

    @clean_unittest
    def test07_Holder05(self):
        ''' Test update holder with equal number of object '''
        person0 = tracking.Person((1,1), (3,3))
        person1 = tracking.Person((5,5), (7,9))
        person2 = tracking.Person((5,5), (5,5))
        person3 = tracking.Person((1,1), (1,1))
        holder = tracking.Holder()
        holder.add(person0)
        holder.add(person1)
        holder.update([person2, person3])
        self.assertTrue(holder.holded[1] is person1)
        self.assertFalse(person1 is person2)
        self.assertFalse(person1==person2)
        self.assertTrue(person1.previous==[(6,7)])
        self.assertTrue(person1.center==(5,5))
        self.assertTrue(holder.holded[0] is person0)
        self.assertFalse(person0 is person3)
        self.assertTrue(person0.previous==[(2,2)])
        
    @clean_unittest
    def test08_Holder06(self):
        ''' Test update tracker with new object '''
        person1 = tracking.Person((5,5), (7,9))
        person2 = tracking.Person((5,5), (5,5))
        person3 = tracking.Person((1,1), (1,1))
        holder = tracking.Holder()
        holder.add(person1)
        holder.update([person2, person3])
        self.assertTrue(len(holder)==2)
        self.assertTrue(person1.previous==[(6,7)])
        self.assertTrue(person1.center==(5,5))
        self.assertTrue(person3 is holder.holded[1])
        
    
if __name__ == '__main__':

    unittest.main(exit=False)